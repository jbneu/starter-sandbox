package xyz.jbneu.spring.starter.standbox.mystarter;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mystarter")
public class MyStarterController {

    private final MyStarterProperties myStarterProperties;

    public MyStarterController(MyStarterProperties myStarterProperties) {
        this.myStarterProperties = myStarterProperties;
    }

    @GetMapping
    public String myStarter() {
        return myStarterProperties.getText();
    }
}
