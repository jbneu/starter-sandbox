package xyz.jbneu.spring.starter.standbox.mystarter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ConditionalOnWebApplication
@ConditionalOnProperty(prefix = "my.starter", value = "enabled", matchIfMissing = true)
@ComponentScan
@Configuration
public class MyStarterAutoConfiguration {
}
