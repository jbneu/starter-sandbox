package xyz.jbneu.spring.starter.standbox.mystarter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@ConfigurationProperties("my.starter")
public class MyStarterProperties {

    /**
     * Enabled by default
     */
    private boolean isEnabled = true;

    /**
     * Allow to customise the text returned by mycontroller
     */
    private String text = "My Starter is auto configured";

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyStarterProperties that = (MyStarterProperties) o;
        return isEnabled == that.isEnabled &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isEnabled, text);
    }

    @Override
    public String toString() {
        return "MyStarterProperties{" +
                "isEnabled=" + isEnabled +
                ", text='" + text + '\'' +
                '}';
    }
}
